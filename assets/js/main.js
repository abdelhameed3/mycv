// $(document).on('contextmenu', function(e) {
 
//   return false;
// });

(function($){
    "use strict";
    $(document).ready(function ($) {
// affix
        $('#stickycard').affix({
            offset: {     
                top: $('#stickycard').offset().top,
                bottom: 50
            }
        });
        $('nav').affix({
            offset: {     
                top: $('nav').offset().top,
                bottom: 50
            }
        });
    });

    
    $(document).ready(function ($) {
        $(".bullet").each(function(){
            var maxValue = $(this).attr("data-max");
            var dataValue = $(this).attr("data-value");
            for(var i =0; i < maxValue; i++){
                $(this).each(function(){
                    $(this).append('<span></span>');
                });
            }
            for(var i=1; i <= dataValue; i++){
                $(this).find("span:nth-child("+i+")").addClass("fill");
            }
        });
        
    });

    // Remove Fixed
    $(document).ready(function ($) {
        $(".alertM i.fa-times").click(function(){
            $(".alertM").hide(200);
        });
    });
    $(document).ready(function (){
   
        $("nav li a").click(function (e){
             e.preventDefault();
              $("html, body").animate({
                  scrollTop: $("#"+ $(this).data("scroll")).offset().top
              }, 1000);
      //Add Class Active
            $(this).addClass("active").parent().siblings().find("a").removeClass("active");
         });
          // Sycn Navbar
          
          $(window).scroll(function (){
              $(".widget").each(function (){
                  if($(window).scrollTop() > ($(this).offset().top - 50)){
                      var sectionID=$(this).attr("id");
                      $("nav li a").removeClass("active");
                      $("nav li a[data-scroll='"+ sectionID +"']").addClass("active");
                  }
              })
          })
      });

})(jQuery);

$(function () { 
    $('[data-toggle="tooltip"]').tooltip({trigger: 'manual'}).tooltip('show');
});  
     
$(".progress-bar").each(function(){
    each_bar_width = $(this).attr('aria-valuenow');
    $(this).width(each_bar_width + '%');
});


// Init WOW.js and get instance
var wow = new WOW();
wow.init();
